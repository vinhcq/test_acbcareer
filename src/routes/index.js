import NewsPage from '../views/NewsPage';
import RankingPage from '../views/RankingPage';
import App from '../App';

var indexRoutes = [
    { path: "/news", name: "Tin tức", component: NewsPage},
    { path: "/ranking", name: "Bảng xếp hạng", component: RankingPage},
    { path: "/", name: "Home",  component: App}
];

export default indexRoutes