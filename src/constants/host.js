var HOST = "";
if (
  window.location.hostname === "localhost" ||
  window.location.hostname === "[::1]" ||
  window.location.hostname.match(
    /^192(?:\.(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)){3}$/
  )
) {
  HOST = "http://acbgame2019.local:8080/api";
} else {
  HOST = window.location.protocol + "//" + window.location.hostname + "/api";
}

export default HOST;
