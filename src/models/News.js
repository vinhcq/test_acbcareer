import callApi from "../utils/callApi";

const News = {
  get() {
    return callApi("blog", "GET").then(res => res.data);
  }
};

export default News;
