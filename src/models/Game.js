import callApi from '../utils/callApi';

const Game = {
  getLevels() {
    return callApi('question-types', 'GET').then(res => res.data);
  },
  getQuestions(id) {
    return callApi('question-list/' + id, 'GET').then(res => res.data);
  },
  getTime(id) {
    return callApi('time-setting', 'GET').then(res => res.data);
  },
  checkAnswer(data) {
    return callApi('question-check', 'POST', data).then(res => res.data);
  },
  begin(id) {
    return callApi('game-start/' + id, 'GET').then(res => res.data);
  },
  end(data) {
    return callApi('game-end', 'POST', data).then(res => res.data);
  },
};

export default Game;
