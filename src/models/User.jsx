import callApi from "../utils/callApi";

const User = {
  getUser() {
    return callApi("profile", "GET").then(res => res.data);
  },
  getCities() {
    return callApi("city", "GET").then(res => res.data);
  },
  getScore() {
    return callApi("score", "GET").then(res => res.data);
  },
  updateUser(data) {
    return callApi("update-profile", "POST", data, true).then(res => res.data);
  },
  getTopUserAllTime() {
    return callApi("top-players", "GET").then(res => res.data);
  },
  getTopUserThisWeek() {
    return callApi("top-players-by-week", "GET").then(res => res.data);
  }
};

export default User;
