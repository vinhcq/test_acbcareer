import React, { Component } from 'react';
import './App.css';
// import { Route, BrowserRouter } from 'react-router-dom'
import LoginPage from './views/LoginPage';
import LevelPage from './views/LevelPage';
// import NewsPage from './views/NewsPage';
// import CharacterPage from './views/CharacterPage';
// import RankingPage from './views/RankingPage';
import GamePage from './views/GamePage';
// import Popup from './views/Popup';
import Authentication from './models/Authentication';
import BACK from './assets/img/btn-back.png';

class App extends Component {
  state = {
    stage: "",
    level: "",
  }
  componentDidMount() {
    var stage = Authentication.checkLogin() ? 1 : 0;
    this.setState({
      stage
    });
  }
  handleOnEnded = () =>{
    this.setState(prevState => ({
      stage: prevState.stage + 1
    }))
  }
  handleOnRegister = data => {
    this.setState({
      level: data.level,
      stage: 4
    });
  };
  handleQuestionFromLevel = data => {
    this.setState({
      level: data.level,
      questions: data.questions,
      stage: 2
    });
  };
  handleOnBack = () =>{
    switch(this.state.stage){
      case 3:
        this.setState({ stage: 1 });
        break;
      case 4:
        this.setState({ stage: 1 });
        break;
      case 5:
        this.setState({ stage: 1 });
        break;
      default: 
        this.setState(prevState =>({
          stage: prevState.stage - 1
        }))
        break;
    }
  }

  renderView(){
    const stage = this.state.stage;
    const props = this.props;

    switch(stage){
      case 0:
        return <LoginPage {...props} handleOnEnded={this.handleOnEnded} />
      case 1:
        return (
          <LevelPage 
          {...props} 
          handleOnRegister = {this.handleOnRegister}
          handleOnEnded={this.handleOnEnded}
          recieveQuestionFromLevel={this.handleQuestionFromLevel} />
        );
      case 2:
        return (
          <GamePage
           handleOnEnded ={this.handleOnEnded}
           handleOnBack  ={this.handleOnBack}    
          />
        )
      default:
    }
  }

  render() {
    return (
      <div className="App">
        {this.state.stage > 0 && (
          <div className="btn-back">
            <img src={BACK} alt="back" onClick={this.handleOnBack}/>
          </div>
        )}
        {this.renderView()}
      </div>
      
  );
  }
  
}

export default App;
