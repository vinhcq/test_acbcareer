import React, { Component } from "react";
import LOGO from '../assets/img/logo.png';
import BANNER_SM from '../assets/img/banner-sm.png';
import BNNV from '../assets/img/banner-nv.png';
import BNLG from '../assets/img/banner-lg.png';
import ICON_USER from '../assets/img/icon-user.png';
import ICON_MAIL from '../assets/img/icon-mail.png';
import ICON_PHONE from '../assets/img/icon-phone.png';

import Authentication from '../models/Authentication';
import { validateEmail, validatePhone } from '../utils/validate'

import { Row,
         Col, 
         Form,} from "reactstrap";
class LoginPage extends Component {
  constructor(props){
    super(props);
    this.state = {
      email: '',
      name: '',
      phone: '',
      isLoading: false,
      isLogin: false
    }
    this.user_ad  = React.createRef();
    this.password = React.createRef();
  }
  componentWillMount() {
    localStorage.clear();
  }

  handleOnSubmit = () =>{
    const name  = this.state.name;
    const email = this.state.email;
    const phone = this.state.phone;
    this.setState({isLoading:true})

    if(!name || !email || !phone){
      this.setState({
        alert: "Vui lòng nhập đầy đủ các trường thông tin trước khi bắt đầu!",
        isLoading: false
      });
    } else if(!validateEmail(email)){
      this.setState({
        alert: "Email không hợp lệ",
        isLoading: false
      })
    } else if(!validatePhone(phone)){
      this.setState({
        alert: "Số điện thoại không hợp lệ",
        isLoading: false
      })
    } else {
      Authentication.login({ name, email, phone }).then(res =>{
          localStorage.setItem("token", res.data.user_token)
          localStorage.setItem("user", res.data.email)
          this.handleOnEnded();
      })  
    }
  }

  handleOnEnded = () =>{
    setTimeout(() =>{
      this.setState({
        isLoading: false
      })
      this.props.handleOnEnded();
    }, 1000)
  };

  handleOnChange = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };
  handleOnPhoneChange = e =>{
    var phone = e.target.value;
    var regex = /[\d]+$/;
    if(regex.test(phone) !== true){
      phone = phone.replace(/[^\d]+/, "");
    }
    this.setState({
      phone
    })
  }
  render() {
    return (
      <div className="view-login fixed animated fadeIn">
        <div className="login-container">
          <div className="acb-logo">
            <img src={LOGO} alt="acb logo" />
          </div>
          <Row>
            <Col
              md="8"
              sm="7"
              xs="12"
              className="col-align-center justify-right">
              <div className="login-left">
                <div className="login-left-container">
                  <div className="banner">
                    <img src={BANNER_SM} className="banner-sm" alt=""/>
                  </div>
                  <div className="banner-nv">
                    <img src={BNNV} alt="nv" />
                  </div>
                </div>
              </div>
            </Col>

            <Col
              md="4"
              sm="5"
              xs="12"
              className="col-align-center justify-center-sm">
                <div className="login-right">
                    <div className="login-right-container">
                        <div className="banner-lg">
                            <img src={BNLG} alt="nv" />
                        </div>
                    </div>
                    <div className="form-login">
                        <Form>
                            <div className="form-control-lg input-group">
                                <div className="input-group-prepend">
                                    <span className="input-group-text">
                                        <img src={ICON_MAIL} alt="mail"/>
                                    </span>
                                    <input type="text"
                                           name="email"
                                           placeholder="Your Mail"
                                           value={this.state.email}
                                           onChange={this.handleOnChange}
                                           className="input-form-control"/>
                                </div>
                            </div>
                            <div className="form-control-lg input-group">
                                <div className="input-group-prepend">
                                    <span className="input-group-text">
                                        <img src={ICON_USER} alt="mail"/>
                                    </span>
                                    <input type="text"
                                           name="name"
                                           placeholder="Your Name"
                                           value={this.state.name}
                                           onChange={this.handleOnChange}
                                           className="input-form-control"/>
                                </div>
                            </div>
                            <div className="form-control-lg input-group">
                                <div className="input-group-prepend">
                                    <span className="input-group-text">
                                        <img src={ICON_PHONE} alt="mail"/>
                                    </span>
                                    <input type="text"
                                           name="phone"
                                           placeholder="Your Phone"
                                           value={this.state.phone}
                                           onChange={this.handleOnChange}
                                           className="input-form-control"/>
                                </div>
                            </div>
                            <div className="message">
                              * Đây là thông tin bắt buộc và không thể chỉnh sửa dành
                               cho việc ứng tuyển và gửi quà tặng, bạn vui lòng kiểm
                               tra chính xác trước khi ấn bắt đầu
                            </div>
                            <div className="text-danger"> {this.state.alert} </div>
                            <button type='button' className="btn-start"
                                    onClick={this.handleOnSubmit}
                            >
                              Bắt Đầu
                            </button>
                        </Form>
                    </div>
                </div>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}

export default LoginPage;
