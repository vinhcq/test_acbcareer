import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import BACK from '../assets/img/btn-back.png';
import ACBLOGO from '../assets/img/logo.png';
import News from '../models/News';
import { countTime } from "../utils/formatDate";

class NewsPage extends Component {
    state = {
        news: []
    };
    componentDidMount(){
        this.getNews();
    }
    getNews = () => {
        News.get().then(res =>{
            this.setState({
                news: res
            });
        });
    }
    handleOnBack = () => {
        this.props.history.push("/")
    }
    render() {
        return (
            <div className="news-page">
                <div className="btn-back-news">
                    <img src={BACK} alt="" onClick={this.handleOnBack}/>
                </div>
               
                
                <div className="view-news">
                    <div className="container">
                        <div className="acb-logo">
                            <img src={ACBLOGO} alt=""/>
                        </div>
                        <div className="news-title text-center">
                            TIN TỨC
                        </div>
                        <div className="news-post">
                            <div className="row">
                                {this.state.news.map((prop, key) =>{
                                    return(
                                        <div className="col-md-4" key={key}>
                                            <div className="news-item">
                                                <small className="news-time">
                                                    {countTime(prop.update_at)}
                                                </small>
                                                <a href={prop.link}>
                                                    <div className="news-thumbnail">
                                                        
                                                    </div>
                                                </a>
                                            <div className="news-description">                        
                                                <div className="descr-title">
                                                    {prop.name}
                                                </div>
                                                <div className="description">
                                                    {prop.description}
                                                </div>
                                            </div>
                                            </div>
                                        </div>
                                    )
                                })}
                                </div>                       
                            </div>
                        </div>
                    </div>
                </div>
        );
    }
}

export default NewsPage;