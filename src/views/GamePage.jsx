import React, { Component } from "react";
import BACK from "../assets/img/btn-back.png";
import LOGO from "../assets/img/logo.png";
import ACBICON from '../assets/img/abc-icon.png';
import SILVER from '../assets/img/crown-silver.png';
import BOYPOINTER from '../assets/img/milestone-icon.png';


class GamePage extends Component {
  render() {
    return (
      <div className="view-game">
        <div className="game-button-back">
          <img src={BACK} alt="" />
        </div>
        <div className="game-acb-logo">
          <img src={LOGO} alt="logo" />
        </div>
        <div className="container">
          <div className="timer">3:00</div>
          <div className="game-container">
            <div className="question-right">
              Số điểm cao nhất: <span className="lead">0</span>
            </div>
            <div className="question">
              <strong>Câu hỏi: </strong>
              <span className="animate fadeIn">
                Hiểu như thế nào về hình ảnh “mẹ yêu thương” trong bài thơ Tiếng
                hát con tàu của Chế Lan Viên?
              </span>
            </div>
            <div className="question-img" />
            <div className="answers">
              <div className="answer animated fadeIn">
                <div className="answer-key">A</div>
                <div className="answer-detail">Một người mẹ Tây Bắc nào đó</div>
              </div>
              <div className="answer animated fadeIn">
                <div className="answer-key">B</div>
                <div className="answer-detail"> Đó là nhân dân, đất nước</div>
              </div>
              <div className="answer animated fadeIn">
                <div className="answer-key">C</div>
                <div className="answer-detail"> Cả ba hình ảnh trên</div>
              </div>
            </div>
          </div>
        </div>

        <div>
          <div className="milestone-crown">
              <img src={SILVER} alt="crown"/>
          </div>
          <div className="game-milestone">
            <div className="milestone-ruler">
              <div className="milestone">10</div>
              <div className="milestone">9</div>
              <div className="milestone">8</div>
              <div className="milestone">7</div>
              <div className="milestone">6</div>
              <div className="milestone">5</div>
              <div className="milestone">4</div>
              <div className="milestone">3</div>
              <div className="milestone">2</div>
              <div className="milestone">1</div>
            </div>
          </div>
        </div>
        {/* <div className="game-milestone">
                <div className="milestone-ruler-active">
                    <div className="milestone ">10</div>
                    <div className="milestone ">9</div>
                    <div className="milestone ">8</div>
                    <div className="milestone ">7</div>
                    <div className="milestone ">6</div>
                    <div className="milestone ">5</div>
                    <div className="milestone ">4</div>
                    <div className="milestone ">3</div>
                    <div className="milestone ">2</div>
                    <div className="milestone ">1</div>
                </div>
        </div> */}
        <div className="milestone-pointer">
          <img src={BOYPOINTER} alt=""/>
        </div>
        <div className="game-acb-icon">
            <img src={ACBICON} alt="acbicon"/>
        </div>
      </div>
    );
  }
}

export default GamePage;
