import React, { Component } from 'react';
import { Link } from 'react-router-dom';
// COMPONENT
import InfoDetail from '../components/InfoDetail/InfoDetail';
import RankingLG from '../components/Ranking/RankingLG';
import RankingSM from '../components/Ranking/RankingSM';
import LevelLG from '../components/Levels/LevelLG';
import LevelSM from '../components/Levels/LevelSM';
import User from '../models/User';
import Game from '../models/Game';
// IMAGE
import BACK from '../assets/img/btn-back.png';
import LOGO from '../assets/img/logo.png';
import NEW from '../assets/img/icon/news.svg';
import NEW_NOTI from '../assets/img/icon/news-noti.svg';
import MORE from '../assets/img/arrow-down.png';
import LESS from '../assets/img/arrow-up.png';

class LevelPage extends Component {
    state ={
        more: false,
        levels: []
    };
    componentDidMount() {
        this.getLevels();
        this.getTopUser();
      }
    handleOnView = () => {
        this.setState(prevState => ({
          more: !prevState.more
        }));
    };
    getLevels = () =>{
        Game.getLevels().then(res =>{
            this.setState({
                levels: res
            })
        })
    }
    handleOnPickLevel = (id) =>{
        const level = this.state.levels.find(prop => {
            return prop.id === id
        })
        Game.getQuestions(id).then(res =>{
            this.props.recieveQuestionFromLevel({
                level: level,
                question: res.data
            })
        })
    }
    // handleOnRegister = id =>{

    // }
    getTopUser = () =>{
        User.getTopUserAllTime().then(res =>{
            this.setState({
                topUser: res.data.splice(0, 3)
            })
        })
    }
    render() {
        return (
            <div>
                {/* <Link to="/login">
                <div className="btn-back">
                    <img src={BACK} alt="back"/>
                </div>
                </Link> */}
                
                <div className="view-level">
                    <div className="level-container">
                        <div className="acb-logo abc-container">
                            <img src={LOGO} alt="acb logo" />
                        </div>    
                    </div>
                   <div className="ranking">
                        <RankingLG topUser={this.state.topUser}/>
                        <RankingSM topUser={this.state.topUser}/>
                   </div>
                    <Link to="/news">
                    <div className="news">
                        <img className="new-noti" src={NEW_NOTI} alt=""/>
                        <img className="new" src={NEW} alt=""/>
                    </div>
                    </Link>
                    
                    <div className="text-center nhomcv">
                        Nhóm công việc/vị trí bạn muốn ứng tuyển
                    </div>
                    <div className="levels">
                        <LevelLG    
                        levels={this.state.levels}
                        recieveQuestionFromLevel={this.handleOnPickLevel}/>

                        <LevelSM 
                        levels={this.state.levels}
                        recieveQuestionFromLevel={this.handleOnPickLevel}/>
                    </div>

                    <div className="view-info-box">
                        <p className="text-center">Thông tin chương trình và luật chơi?</p>
                        <div className={"info-detail " + (this.state.more ? "" : "hidden")}>
                            <InfoDetail more={this.state.more}/>                    
                        </div>
                        {this.state.more ? (
                            <div className="view-less-info">
                                <img onClick={this.handleOnView} src={LESS} alt="less" />
                                <div>Thu gọn</div>
                            </div>
                        ) : (
                        <div className="view-more-info">
                            <div>Click tại đây để xem chi tiết</div>
                            <img onClick={this.handleOnView} src={MORE} alt="more" />
                        </div>
                        )}
                    </div>
                </div>
            </div>
            
            
        );
    }
}

export default LevelPage;