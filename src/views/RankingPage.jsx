import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import BACK from '../assets/img/btn-back.png';
import ACBWHITE from '../assets/img/logo-white.png';
import CUP from '../assets/img/cup.png';
import GOLD from '../assets/img/crown-gold.png';
import SILVER from '../assets/img/crown-silver.png';
import BRONZE from '../assets/img/crown-bronze.png';
import User from '../models/User';

class RankingPage extends Component {
    state ={
        topUser: [{ name: "", sum: 0 }],
        user: [{ order: null, name: null, sum: null }]
    }
    
    handleChangeTab = params => {
        if (params === "week") {
            this.getTopUserThisWeek();
          } else {
            this.getTopUserAllTime();
          }
        this.setState(state => ({
          tab: !state.tab
        }));
    };
    componentDidMount() {
        this.getTopUserThisWeek();
        this.getUserScore();
      }
    
      getUserScore = () => {
        User.getScore().then(res => {
          console.log(res);
          this.setState({
            user: res[0]
          });
        });
      };
    
      getTopUserAllTime = () => {
        User.getTopUserAllTime().then(res => {
          this.setState({
            topUser: res.data.splice(0, 20)
          });
        });
      };
    
      getTopUserThisWeek = () => {
        User.getTopUserThisWeek().then(res => {
          this.setState({
            topUser: res.data.splice(0, 20)
          });
        });
      };
    
    handleOnBack = () => {
        this.props.history.push("/")
    }
    render() {
      const topUser = this.state.topUser;
      const user = this.state.user;
      const champion = topUser.length > 0 ? topUser[0] : { name: "", sum: 0 };
        return (
            <div className="ranking">
                <div className="btn-back-ranking">
                    <img src={BACK} alt="" onClick={this.handleOnBack}/>
                </div>
                
                <div className="view-ranking">
                    <div className="ranking-top">
                        <div className="container">
                            <div className="logo-acb-ranking">
                                <img src={ACBWHITE} alt=""/>
                            </div>
                            <div className="ranking-title text-center">
                                BẢNG XẾP HẠNG
                            </div>
                            <div className="ranking-tab">
                                <div className={"ranking-tab-item " + (!this.state.tab ? "tab-active" : "")} 
                                     onClick={() => this.handleChangeTab("week")}>
                                TOP TUẦN
                                </div>
                                <div className={"ranking-tab-item " + (this.state.tab ? "tab-active" : "")} 
                                    onClick={() => this.handleChangeTab("all")}>
                                TOÀN BỘ
                                </div>
                            </div>
                            <div className="champion-container">
                                <div className="cup sm-hidden">
                                    <img src={CUP} alt="cup"/>
                                </div>
                                <div className="champion">
                                    <div className="champion-name">
                                    {/* Trịnh Hoài Đức */}
                                    {champion.name}
                                    </div>
                                    <div className="champion-sum">
                                    {champion.sum}
                                    </div>
                                </div>
                                <div className="cup">
                                    <img src={CUP} alt="cup"/>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <div className="ranking-body">
                        <div className="container">
                            <div className="ranking-body-list">
                                {/* <div className="ranking-list-item">
                                    <div className="position">
                                        1
                                    </div>
                                    <div className="ranking-detail">
                                        <div className="detail-name">
                                            Trịnh Hoài Đức
                                        </div>
                                        <div className="detail-crown">
                                            <img src={GOLD} alt=""/>
                                        </div>
                                        <div className="detail-sum">
                                            1000
                                        </div>
                                    </div>
                                </div> */}
                                {topUser.map((user,key) =>{
                                    if(user.sum === null) return "";
                                    return(
                                        <div className="ranking-list-item" key={key}>
                                            <div className="position">
                                                {key+1}
                                            </div>
                                            <div className="ranking-detail">
                                                <div className="detail-name">
                                                    {user.name}
                                                </div>
                                                <div className="detail-crown">
                                                    <img src={
                                                        key === 0
                                                        ? GOLD
                                                        : key === 1
                                                        ? SILVER
                                                        : key === 2
                                                        ? BRONZE
                                                        : ""
                                                    } alt=""/>
                                                </div>
                                                <div className="detail-sum">
                                                    {user.sum}
                                                </div>
                                            </div>
                                        </div>
                                    )
                                })}
                            </div>
                        </div>
                    </div>

                    <div className="ranking-footer">
                        <div className="container">
                            <div className="ranking-text-desc">
                                Vị trí của bạn
                            </div>
                            <div className="your-ranking-item">
                                <div className="your-pos">
                                    {user.order}
                                </div>
                                <div className="your-name">
                                    {user.name}
                                </div>
                                <div className="your-crown">
                                    <img src={GOLD} alt=""/>
                                </div>
                                <div className="your-sum">
                                    {user.sum}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default RankingPage;
RankingPage.propTypes = {};
RankingPage.defaultProps = {};