import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import BACK from '../assets/img/btn-back.png';
import LOGO from '../assets/img/logo.png';
import BOY from '../assets/img/character-boy.png';
import GIRL from '../assets/img/character-girl.png';
import ABC_ICON from '../assets/img/abc-icon.png';

class CharacterPage extends Component {
    state = {
        character: "boy"
    };

    handleOnClick = params => {
        this.setState({
          character: params
        });
    };
    render() {
        const character = this.state.character;

        return (
            <div>   
                <div className="btn-back">
                    <Link to="/level">
                    <img src={BACK} alt="back"/>
                    </Link>
                    
                </div>
                <div className="view-character">
                    <div className="character-container">
                        <div className="character-acb-logo">
                            <img src={LOGO} alt="acb logo" />
                        </div>
                       <div className="title-character">
                           Fresher
                       </div>
                       <div className="text-center my-4">
                           CHỌN NHÂN VẬT
                       </div>
                       <div className="characters">
                           <div className={"character " + (character === "boy" ? "character-focus" : "")}
                                onClick={() => this.handleOnClick("boy")}>
                                <img src={BOY} alt="boy"/>
                           </div>
                           <div className={"character " + (character === "girl" ? "character-focus" : "")}
                                onClick={() => this.handleOnClick("girl")}>
                                <img src={GIRL} alt="girl"/>
                           </div>
                       </div>
                       <div className="text-center">
                            Số câu trả lời đúng cao nhất: 0
                       </div>
                       <div className="button-container text-center">
                            <button className="btn-round btn-start-character">
                                Bắt Đầu
                            </button>
                       </div>
                    </div>

                    <div className="abc-icon">
                        <img src={ABC_ICON} alt="abcicon"/>
                    </div>
                </div>
            </div>
        );
    }
}

export default CharacterPage;