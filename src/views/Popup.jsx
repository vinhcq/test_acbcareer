import React, { Component } from 'react';
import SUCCESS from '../assets/img/popup-success-bg.png'
import FAIL from '../assets/img/popup-fail-bg.png';
import POINTER from '../assets/img/pointer.png';

class Popup extends Component {
    render() {
        return(     
            <div className="popup">
                <div className="popup-container">
                    <img src={SUCCESS} alt="" className="popup-bg"/>
                        <div className="popup-content">
                            Chúc mừng bạn đã vượt qua 10 câu hỏi và
                            tiến trực tiếp đến vòng
                            ứng tuyển của chúng tôi !
                            <div className="popup-link">
                                CHƠI LẠI <br/>
                                <img src={POINTER} alt=""/>
                            </div> 
                            
                        </div>
                        <div className="popup-button">
                                <div className="popup-btn-home">
                                    Trang chủ
                                </div>
                        </div>                    
                </div>

            </div>            
        )
        
    }
}

export default Popup;