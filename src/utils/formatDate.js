import moment from "moment";

export const countTime = dateString => {
  const date = new moment(dateString).fromNow();

  return date;
};
