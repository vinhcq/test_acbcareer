import React from "react";

const InfoDetail = props => {
  return (
    <div>
      <ul>
        <li>
          <div className="title">ĐỐI TÁC SỰ NGHIỆP</div>
          Khởi đầu giai đoạn tăng tốc phát triển toàn diện 2019 - 2024, ACB tập
          trung tăng cường chất lượng nguồn nhân lực để tạo nên sức bật vượt
          trội và bền vững theo chiến lược của ngân hàng. ACB triển khai chương
          trình tìm kiếm “Đối tác sự nghiệp”, mong muốn trở thành đối tác với
          nhân tài trong nhiều lĩnh vực chuyên môn khác nhau. <br />
          <br />
          Dù bạn là sinh viên mới tốt nghiệp, nhân sự đã có kinh nghiệm hoặc
          từng đảm nhận vị trí quản lý, ACB luôn có cơ hội và thử thách phù hợp
          với Bạn. <br />
          Trở thành Đối Tác Sự Nghiệp của ACB, chúng ta có thể cùng nhau: <br />
          <ul>
            <li>Học hỏi và trải nghiệm những điều mới</li>
            <li>Chủ động lựa chọn con đường thăng tiến</li>
            <li>Nỗ lực hướng đến thành tựu chung của tổ chức</li>
            <li>Phát triển và gặt hái thành công</li>
          </ul>
          <br />
        </li>
        <li>
          <div className="title">ACB CHALLENGING GAME</div>
          Nếu bạn đã sẵn sàng trở thành Đối Tác Sự Nghiệp của ACB, hãy thể thể
          hiện tài năng và bản lĩnh của mình để đến với vòng phỏng vấn của ACB
          mà không cần trải qua vòng thi test với thử thách ACB Challenging
          Game. <br /><br />
          <h5>Cách thức tham gia:</h5>
          <ul>
            <li>Bạn có 3 phút để trả lời những câu hỏi ngẫu nhiên</li>
            <li>
              Mỗi câu trả lời đúng liên tiếp, bạn sẽ tiến lên 1 nấc. Nếu sai,
              bạn sẽ quay trở lại vạch xuất phát
            </li>
            <li>
              Khi trả lời đúng 10 câu liên tiếp, bạn có cơ hội đến với vòng
              phỏng vấn (*) và nhận nhiều phần quà hấp dẫn
            </li>
          </ul>
          Lưu ý:
          <br />
          <ul>
            <li>
              Mỗi ngày chúng tội dành tặng bạn 1 lượt chơi và khởi động vào
              7:00AM mỗi ngày. Nếu hôm nay bạn chưa hoàn thành được thử thách
              thì hãy ôn luyện thật kỹ để “chiến” vào ngày tiếp theo nhé.{" "}
            </li>
            <li>
              (*) Khi hoàn thành đúng 10 câu trả lời liên tiếp, bạn vui lòng
              chọn nút “Ứng tuyển” và điển đầy đủ các thông tin yêu cầu. Chúng
              tôi sẽ xem xét sự phù hợp của bạn với vị trí ứng tuyển để mời bạn
              đến với vòng phỏng vấn của chương trình.
            </li>
          </ul>
          <h5>Giải thưởng:</h5>
          <ul>
            <li>
              Giải thưởng tuần: 05 Voucher xem phim CGV dành cho TOP 5 người
              chơi có điểm số cao nhất mỗi tuần
            </li>
            <li>
              Giải tổng kết: 01 Máy chụp ảnh lấy liền dành cho 01 chiến binh có
              điểm số cao nhất (tính đến thời điểm tổng kết)
            </li>
            <li>
              Lưu ý: Trong trường hợp người chơi có cùng điểm số, BTC sẽ căn cứ
              vào thời gian trả lời đúng nhanh nhất để gửi giải thưởng.
            </li>
          </ul>
          Chúc bạn thành công!
        </li>
      </ul>
    </div>
  );
};

export default InfoDetail;
