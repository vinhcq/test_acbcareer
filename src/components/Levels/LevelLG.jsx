import React from "react";

const LevelLG = () => {
  return (
    <div className="level-list-lga">
      <div className="level-item">
        <div className="level-bg fresher">
          <div className="level-desc">
            <div className="level-name">Fresher</div>
            <div className="level-score">Điểm: 0</div>
          </div>
        </div>
      </div>
      <div className="level-item">
        <div className="level-bg junior">
          <div className="level-desc">
            <div className="level-name">Junior</div>
            <div className="level-score">Điểm: 0</div>
          </div>
        </div>
      </div>
      <div className="level-item">
        <div className="level-bg senior">
          <div className="level-desc">
            <div className="level-name">Senior</div>
            <div className="level-score">Điểm: 0</div>
          </div>
        </div>
      </div>
      <div className="level-item">
        <div className="level-bg it">
          <div className="level-desc">
            <div className="level-name">IT</div>
            <div className="level-score">Điểm: 0</div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default LevelLG;
