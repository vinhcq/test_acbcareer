import React from "react";
import { Link } from "react-router-dom";
import GOLD from "../../assets/img/crown-gold.png";
import SILVER from "../../assets/img/crown-silver.png";
import BRONZE from "../../assets/img/crown-bronze.png";

const RankingSM = (props) => {
    const topUser = props.topUser
    return (
    <div className="ranking-sm">
        {topUser.map((user, key) => {
            if(user.sum === 0) return;
            return(
                <div className="ranking-item" key={key}>
                <div className="row">
                    <div className="text-left col-7">
                        {user.name}
                    </div>
                    <div className="col-2">
                        <img 
                                src={
                                    key === 0
                                    ?   GOLD
                                    :   key === 1
                                    ?   SILVER
                                    :   BRONZE
                                } 
                        alt=""/>
                    </div>
                    <div className="col-3">
                        {user.sum}
                    </div>
                </div>
        </div>
        )         
        })}      
        <span>TOP 3</span>
        <Link to="/ranking" className="view-more">
          Xem thêm
        </Link>
    </div>
  );
};

export default RankingSM;
RankingSM.propTypes = {};
RankingSM.defaultProps = {
  topUser: [],
};
