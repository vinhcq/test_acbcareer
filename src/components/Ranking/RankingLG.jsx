import React from "react";
import { Link } from "react-router-dom";
import GOLD from "../../assets/img/crown-gold.png";
import SILVER from "../../assets/img/crown-silver.png";
import BRONZE from "../../assets/img/crown-bronze.png";

const RankingLG = (props) => {
  const topUser = props.topUser;

  return (
    <div className="ranking-lg">
      <div className="ranking-bg">
        <span>TOP 3</span>
        <Link to="/ranking" className="view-more">
          Xem thêm
        </Link>
      </div>
      <div className="ranking-list">
        <div className="row rank-row">
          {topUser.map((user, key) => {
            if (user.num === null) return "";
            return (
              <div className="col-md-4" key={key}>
                <div className="ranking-item">
                  <img 
                        src={
                          key === 0 
                          ? GOLD
                          : key === 1
                          ? SILVER
                          : BRONZE
                        }/>
                  <div className="ranking-name">{user.name}</div>
                  <div className="ranking-score">{user.sum}</div>
                </div>
              </div>
            );
          })}
        </div>
      </div>
    </div>
  );
};

export default RankingLG;
RankingLG.propTypes = {};
RankingLG.defaultProps = {
  topUser: [],
};
